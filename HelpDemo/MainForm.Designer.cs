﻿namespace HelpDemo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel100 = new System.Windows.Forms.Panel();
            this.Panel1Label = new System.Windows.Forms.Label();
            this.Panel1ComboBox = new System.Windows.Forms.ComboBox();
            this.Panel1Button = new System.Windows.Forms.Button();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.menu1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenu51ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenu52ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel200 = new System.Windows.Forms.Panel();
            this.Panel2CheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.Panel2TextBox = new System.Windows.Forms.TextBox();
            this.Panel2Label = new System.Windows.Forms.Label();
            this.panel300 = new System.Windows.Forms.Panel();
            this.Panel3ListBox = new System.Windows.Forms.ListBox();
            this.Panel3NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.Panel3Label = new System.Windows.Forms.Label();
            this.panel400 = new System.Windows.Forms.Panel();
            this.Panel4RadioButton2 = new System.Windows.Forms.RadioButton();
            this.Panel4RadioButton1 = new System.Windows.Forms.RadioButton();
            this.Panel4RichTextBox = new System.Windows.Forms.RichTextBox();
            this.Panel4Label = new System.Windows.Forms.Label();
            this.panel500 = new System.Windows.Forms.Panel();
            this.Panel5MonthCalendar = new System.Windows.Forms.MonthCalendar();
            this.Panel5Label = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.helpProvider = new System.Windows.Forms.HelpProvider();
            this.radOnline = new System.Windows.Forms.RadioButton();
            this.radOffline = new System.Windows.Forms.RadioButton();
            this.Panel600 = new System.Windows.Forms.Panel();
            this.Panel600Button = new System.Windows.Forms.Button();
            this.Panel6Label = new System.Windows.Forms.Label();
            this.txtOfflineUrl = new System.Windows.Forms.TextBox();
            this.txtOnlineUrl = new System.Windows.Forms.TextBox();
            this.LocationLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel100.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.panel200.SuspendLayout();
            this.panel300.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Panel3NumericUpDown)).BeginInit();
            this.panel400.SuspendLayout();
            this.panel500.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.Panel600.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel100
            // 
            this.panel100.Controls.Add(this.Panel1Label);
            this.panel100.Controls.Add(this.Panel1ComboBox);
            this.panel100.Controls.Add(this.Panel1Button);
            this.helpProvider.SetHelpNavigator(this.panel100, System.Windows.Forms.HelpNavigator.Topic);
            this.panel100.Location = new System.Drawing.Point(20, 92);
            this.panel100.Margin = new System.Windows.Forms.Padding(4);
            this.panel100.Name = "panel100";
            this.helpProvider.SetShowHelp(this.panel100, true);
            this.panel100.Size = new System.Drawing.Size(319, 342);
            this.panel100.TabIndex = 0;
            this.panel100.Tag = "100";
            // 
            // Panel1Label
            // 
            this.Panel1Label.AutoSize = true;
            this.Panel1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel1Label.Location = new System.Drawing.Point(6, 0);
            this.Panel1Label.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.Panel1Label.Name = "Panel1Label";
            this.Panel1Label.Size = new System.Drawing.Size(111, 39);
            this.Panel1Label.TabIndex = 2;
            this.Panel1Label.Text = "About";
            // 
            // Panel1ComboBox
            // 
            this.Panel1ComboBox.FormattingEnabled = true;
            this.helpProvider.SetHelpNavigator(this.Panel1ComboBox, System.Windows.Forms.HelpNavigator.Topic);
            this.Panel1ComboBox.Items.AddRange(new object[] {
            "Safety Information"});
            this.Panel1ComboBox.Location = new System.Drawing.Point(28, 200);
            this.Panel1ComboBox.Margin = new System.Windows.Forms.Padding(6);
            this.Panel1ComboBox.Name = "Panel1ComboBox";
            this.helpProvider.SetShowHelp(this.Panel1ComboBox, false);
            this.Panel1ComboBox.Size = new System.Drawing.Size(219, 32);
            this.Panel1ComboBox.TabIndex = 1;
            this.Panel1ComboBox.Tag = "102";
            // 
            // Panel1Button
            // 
            this.helpProvider.SetHelpKeyword(this.Panel1Button, "");
            this.helpProvider.SetHelpNavigator(this.Panel1Button, System.Windows.Forms.HelpNavigator.Topic);
            this.Panel1Button.Location = new System.Drawing.Point(28, 108);
            this.Panel1Button.Margin = new System.Windows.Forms.Padding(6);
            this.Panel1Button.Name = "Panel1Button";
            this.helpProvider.SetShowHelp(this.Panel1Button, false);
            this.Panel1Button.Size = new System.Drawing.Size(204, 42);
            this.Panel1Button.TabIndex = 0;
            this.Panel1Button.Tag = "101";
            this.Panel1Button.Text = "Features";
            this.Panel1Button.UseVisualStyleBackColor = true;
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu1ToolStripMenuItem,
            this.menu2ToolStripMenuItem,
            this.menu3ToolStripMenuItem,
            this.menu4ToolStripMenuItem,
            this.menu5ToolStripMenuItem,
            this.configToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.helpProvider.SetShowHelp(this.menuStrip, false);
            this.menuStrip.Size = new System.Drawing.Size(1718, 38);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip1";
            // 
            // menu1ToolStripMenuItem
            // 
            this.menu1ToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.menu1ToolStripMenuItem.Name = "menu1ToolStripMenuItem";
            this.menu1ToolStripMenuItem.Size = new System.Drawing.Size(88, 34);
            this.menu1ToolStripMenuItem.Tag = "100";
            this.menu1ToolStripMenuItem.Text = "About";
            this.menu1ToolStripMenuItem.ToolTipText = "Home Menu";
            this.menu1ToolStripMenuItem.Click += new System.EventHandler(this.MenuToolStripMenu_Click);
            // 
            // menu2ToolStripMenuItem
            // 
            this.menu2ToolStripMenuItem.BackColor = System.Drawing.Color.LawnGreen;
            this.menu2ToolStripMenuItem.Name = "menu2ToolStripMenuItem";
            this.menu2ToolStripMenuItem.Size = new System.Drawing.Size(94, 34);
            this.menu2ToolStripMenuItem.Tag = "200";
            this.menu2ToolStripMenuItem.Text = "Theory";
            this.menu2ToolStripMenuItem.Click += new System.EventHandler(this.MenuToolStripMenu_Click);
            // 
            // menu3ToolStripMenuItem
            // 
            this.menu3ToolStripMenuItem.Name = "menu3ToolStripMenuItem";
            this.menu3ToolStripMenuItem.Size = new System.Drawing.Size(157, 34);
            this.menu3ToolStripMenuItem.Tag = "300";
            this.menu3ToolStripMenuItem.Text = "Programming";
            this.menu3ToolStripMenuItem.Click += new System.EventHandler(this.MenuToolStripMenu_Click);
            // 
            // menu4ToolStripMenuItem
            // 
            this.menu4ToolStripMenuItem.Name = "menu4ToolStripMenuItem";
            this.menu4ToolStripMenuItem.Size = new System.Drawing.Size(93, 34);
            this.menu4ToolStripMenuItem.Tag = "400";
            this.menu4ToolStripMenuItem.Text = "Debug";
            this.menu4ToolStripMenuItem.Click += new System.EventHandler(this.MenuToolStripMenu_Click);
            // 
            // menu5ToolStripMenuItem
            // 
            this.menu5ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenu51ToolStripMenuItem,
            this.subMenu52ToolStripMenuItem});
            this.menu5ToolStripMenuItem.Name = "menu5ToolStripMenuItem";
            this.menu5ToolStripMenuItem.Size = new System.Drawing.Size(118, 34);
            this.menu5ToolStripMenuItem.Tag = "500";
            this.menu5ToolStripMenuItem.Text = "Examples";
            this.menu5ToolStripMenuItem.Click += new System.EventHandler(this.MenuToolStripMenu_Click);
            // 
            // subMenu51ToolStripMenuItem
            // 
            this.subMenu51ToolStripMenuItem.Name = "subMenu51ToolStripMenuItem";
            this.subMenu51ToolStripMenuItem.Size = new System.Drawing.Size(225, 40);
            this.subMenu51ToolStripMenuItem.Tag = "511";
            this.subMenu51ToolStripMenuItem.Text = "Workbook";
            // 
            // subMenu52ToolStripMenuItem
            // 
            this.subMenu52ToolStripMenuItem.Name = "subMenu52ToolStripMenuItem";
            this.subMenu52ToolStripMenuItem.Size = new System.Drawing.Size(225, 40);
            this.subMenu52ToolStripMenuItem.Tag = "512";
            this.subMenu52ToolStripMenuItem.Text = "VBT";
            // 
            // configToolStripMenuItem
            // 
            this.configToolStripMenuItem.Name = "configToolStripMenuItem";
            this.configToolStripMenuItem.Size = new System.Drawing.Size(92, 34);
            this.configToolStripMenuItem.Tag = "600";
            this.configToolStripMenuItem.Text = "Config";
            this.configToolStripMenuItem.Click += new System.EventHandler(this.MenuToolStripMenu_Click);
            // 
            // panel200
            // 
            this.panel200.Controls.Add(this.Panel2CheckedListBox);
            this.panel200.Controls.Add(this.Panel2TextBox);
            this.panel200.Controls.Add(this.Panel2Label);
            this.helpProvider.SetHelpKeyword(this.panel200, "");
            this.helpProvider.SetHelpNavigator(this.panel200, System.Windows.Forms.HelpNavigator.Topic);
            this.panel200.Location = new System.Drawing.Point(20, 470);
            this.panel200.Margin = new System.Windows.Forms.Padding(4);
            this.panel200.Name = "panel200";
            this.helpProvider.SetShowHelp(this.panel200, true);
            this.panel200.Size = new System.Drawing.Size(319, 348);
            this.panel200.TabIndex = 2;
            this.panel200.Tag = "200";
            // 
            // Panel2CheckedListBox
            // 
            this.Panel2CheckedListBox.FormattingEnabled = true;
            this.helpProvider.SetHelpKeyword(this.Panel2CheckedListBox, "");
            this.helpProvider.SetHelpNavigator(this.Panel2CheckedListBox, System.Windows.Forms.HelpNavigator.Topic);
            this.Panel2CheckedListBox.Items.AddRange(new object[] {
            "Test Head Connections, UVS64"});
            this.Panel2CheckedListBox.Location = new System.Drawing.Point(13, 99);
            this.Panel2CheckedListBox.Margin = new System.Windows.Forms.Padding(6);
            this.Panel2CheckedListBox.Name = "Panel2CheckedListBox";
            this.helpProvider.SetShowHelp(this.Panel2CheckedListBox, false);
            this.Panel2CheckedListBox.Size = new System.Drawing.Size(458, 238);
            this.Panel2CheckedListBox.TabIndex = 4;
            this.Panel2CheckedListBox.Tag = "202";
            // 
            // Panel2TextBox
            // 
            this.Panel2TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.helpProvider.SetHelpKeyword(this.Panel2TextBox, "AssociateIndex");
            this.helpProvider.SetHelpNavigator(this.Panel2TextBox, System.Windows.Forms.HelpNavigator.Topic);
            this.Panel2TextBox.Location = new System.Drawing.Point(13, 58);
            this.Panel2TextBox.Margin = new System.Windows.Forms.Padding(6);
            this.Panel2TextBox.Name = "Panel2TextBox";
            this.helpProvider.SetShowHelp(this.Panel2TextBox, false);
            this.Panel2TextBox.Size = new System.Drawing.Size(292, 29);
            this.Panel2TextBox.TabIndex = 3;
            this.Panel2TextBox.Tag = "201";
            this.Panel2TextBox.Text = "Board Hardware";
            // 
            // Panel2Label
            // 
            this.Panel2Label.AutoSize = true;
            this.Panel2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel2Label.Location = new System.Drawing.Point(6, 0);
            this.Panel2Label.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.Panel2Label.Name = "Panel2Label";
            this.Panel2Label.Size = new System.Drawing.Size(337, 39);
            this.Panel2Label.TabIndex = 2;
            this.Panel2Label.Text = "Theory of Operation";
            // 
            // panel300
            // 
            this.panel300.Controls.Add(this.Panel3ListBox);
            this.panel300.Controls.Add(this.Panel3NumericUpDown);
            this.panel300.Controls.Add(this.Panel3Label);
            this.helpProvider.SetHelpKeyword(this.panel300, "TopicId");
            this.helpProvider.SetHelpNavigator(this.panel300, System.Windows.Forms.HelpNavigator.TopicId);
            this.panel300.Location = new System.Drawing.Point(898, 96);
            this.panel300.Margin = new System.Windows.Forms.Padding(4);
            this.panel300.Name = "panel300";
            this.helpProvider.SetShowHelp(this.panel300, true);
            this.panel300.Size = new System.Drawing.Size(427, 280);
            this.panel300.TabIndex = 3;
            this.panel300.Tag = "300";
            // 
            // Panel3ListBox
            // 
            this.Panel3ListBox.FormattingEnabled = true;
            this.Panel3ListBox.ItemHeight = 24;
            this.Panel3ListBox.Items.AddRange(new object[] {
            "Programming Procedure Overview"});
            this.Panel3ListBox.Location = new System.Drawing.Point(13, 140);
            this.Panel3ListBox.Margin = new System.Windows.Forms.Padding(6);
            this.Panel3ListBox.Name = "Panel3ListBox";
            this.Panel3ListBox.Size = new System.Drawing.Size(217, 100);
            this.Panel3ListBox.TabIndex = 4;
            this.Panel3ListBox.Tag = "302";
            // 
            // Panel3NumericUpDown
            // 
            this.Panel3NumericUpDown.Location = new System.Drawing.Point(13, 66);
            this.Panel3NumericUpDown.Margin = new System.Windows.Forms.Padding(6);
            this.Panel3NumericUpDown.Name = "Panel3NumericUpDown";
            this.Panel3NumericUpDown.Size = new System.Drawing.Size(220, 29);
            this.Panel3NumericUpDown.TabIndex = 3;
            this.Panel3NumericUpDown.Tag = "301";
            // 
            // Panel3Label
            // 
            this.Panel3Label.AutoSize = true;
            this.Panel3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel3Label.Location = new System.Drawing.Point(6, 0);
            this.Panel3Label.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.Panel3Label.Name = "Panel3Label";
            this.Panel3Label.Size = new System.Drawing.Size(232, 39);
            this.Panel3Label.TabIndex = 2;
            this.Panel3Label.Text = "Programming";
            // 
            // panel400
            // 
            this.panel400.Controls.Add(this.Panel4RadioButton2);
            this.panel400.Controls.Add(this.Panel4RadioButton1);
            this.panel400.Controls.Add(this.Panel4RichTextBox);
            this.panel400.Controls.Add(this.Panel4Label);
            this.helpProvider.SetHelpNavigator(this.panel400, System.Windows.Forms.HelpNavigator.Topic);
            this.panel400.Location = new System.Drawing.Point(446, 92);
            this.panel400.Margin = new System.Windows.Forms.Padding(4);
            this.panel400.Name = "panel400";
            this.helpProvider.SetShowHelp(this.panel400, true);
            this.panel400.Size = new System.Drawing.Size(359, 342);
            this.panel400.TabIndex = 4;
            this.panel400.Tag = "400";
            // 
            // Panel4RadioButton2
            // 
            this.Panel4RadioButton2.AutoSize = true;
            this.Panel4RadioButton2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.helpProvider.SetHelpNavigator(this.Panel4RadioButton2, System.Windows.Forms.HelpNavigator.Topic);
            this.Panel4RadioButton2.Location = new System.Drawing.Point(0, 284);
            this.Panel4RadioButton2.Margin = new System.Windows.Forms.Padding(6);
            this.Panel4RadioButton2.Name = "Panel4RadioButton2";
            this.helpProvider.SetShowHelp(this.Panel4RadioButton2, true);
            this.Panel4RadioButton2.Size = new System.Drawing.Size(359, 29);
            this.Panel4RadioButton2.TabIndex = 5;
            this.Panel4RadioButton2.TabStop = true;
            this.Panel4RadioButton2.Tag = "403";
            this.Panel4RadioButton2.Text = "DCVS Debug Display Interface";
            this.Panel4RadioButton2.UseVisualStyleBackColor = true;
            // 
            // Panel4RadioButton1
            // 
            this.Panel4RadioButton1.AutoSize = true;
            this.Panel4RadioButton1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.helpProvider.SetHelpKeyword(this.Panel4RadioButton1, "");
            this.helpProvider.SetHelpNavigator(this.Panel4RadioButton1, System.Windows.Forms.HelpNavigator.Topic);
            this.Panel4RadioButton1.Location = new System.Drawing.Point(0, 313);
            this.Panel4RadioButton1.Margin = new System.Windows.Forms.Padding(6);
            this.Panel4RadioButton1.Name = "Panel4RadioButton1";
            this.helpProvider.SetShowHelp(this.Panel4RadioButton1, true);
            this.Panel4RadioButton1.Size = new System.Drawing.Size(359, 29);
            this.Panel4RadioButton1.TabIndex = 4;
            this.Panel4RadioButton1.TabStop = true;
            this.Panel4RadioButton1.Tag = "402";
            this.Panel4RadioButton1.Text = "Setting the Output Current Range and Meter Ranges";
            this.Panel4RadioButton1.UseVisualStyleBackColor = true;
            // 
            // Panel4RichTextBox
            // 
            this.Panel4RichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helpProvider.SetHelpNavigator(this.Panel4RichTextBox, System.Windows.Forms.HelpNavigator.Topic);
            this.Panel4RichTextBox.Location = new System.Drawing.Point(0, 39);
            this.Panel4RichTextBox.Margin = new System.Windows.Forms.Padding(6);
            this.Panel4RichTextBox.Name = "Panel4RichTextBox";
            this.helpProvider.SetShowHelp(this.Panel4RichTextBox, true);
            this.Panel4RichTextBox.Size = new System.Drawing.Size(359, 303);
            this.Panel4RichTextBox.TabIndex = 3;
            this.Panel4RichTextBox.Tag = "401";
            this.Panel4RichTextBox.Text = "Starting the DCVS Debug Display";
            // 
            // Panel4Label
            // 
            this.Panel4Label.AutoSize = true;
            this.Panel4Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel4Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel4Label.Location = new System.Drawing.Point(0, 0);
            this.Panel4Label.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.Panel4Label.Name = "Panel4Label";
            this.Panel4Label.Size = new System.Drawing.Size(253, 39);
            this.Panel4Label.TabIndex = 2;
            this.Panel4Label.Text = "Debug Display";
            // 
            // panel500
            // 
            this.panel500.CausesValidation = false;
            this.panel500.Controls.Add(this.Panel5MonthCalendar);
            this.panel500.Controls.Add(this.Panel5Label);
            this.panel500.Location = new System.Drawing.Point(446, 500);
            this.panel500.Margin = new System.Windows.Forms.Padding(9, 10, 9, 10);
            this.panel500.Name = "panel500";
            this.helpProvider.SetShowHelp(this.panel500, false);
            this.panel500.Size = new System.Drawing.Size(359, 228);
            this.panel500.TabIndex = 5;
            this.panel500.Tag = "500";
            // 
            // Panel5MonthCalendar
            // 
            this.Panel5MonthCalendar.Location = new System.Drawing.Point(17, 60);
            this.Panel5MonthCalendar.Margin = new System.Windows.Forms.Padding(17, 16, 17, 16);
            this.Panel5MonthCalendar.Name = "Panel5MonthCalendar";
            this.Panel5MonthCalendar.TabIndex = 3;
            this.Panel5MonthCalendar.Tag = "501";
            // 
            // Panel5Label
            // 
            this.Panel5Label.AutoSize = true;
            this.Panel5Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel5Label.Location = new System.Drawing.Point(6, 0);
            this.Panel5Label.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.Panel5Label.Name = "Panel5Label";
            this.Panel5Label.Size = new System.Drawing.Size(175, 39);
            this.Panel5Label.TabIndex = 2;
            this.Panel5Label.Text = "Examples";
            // 
            // pictureBox
            // 
            this.pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox.Image")));
            this.pictureBox.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox.InitialImage")));
            this.pictureBox.Location = new System.Drawing.Point(0, 808);
            this.pictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox.Name = "pictureBox";
            this.helpProvider.SetShowHelp(this.pictureBox, false);
            this.pictureBox.Size = new System.Drawing.Size(1718, 150);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            // 
            // radOnline
            // 
            this.radOnline.AutoSize = true;
            this.radOnline.Checked = true;
            this.radOnline.Location = new System.Drawing.Point(227, 76);
            this.radOnline.Margin = new System.Windows.Forms.Padding(6);
            this.radOnline.Name = "radOnline";
            this.radOnline.Size = new System.Drawing.Size(94, 29);
            this.radOnline.TabIndex = 6;
            this.radOnline.TabStop = true;
            this.radOnline.Tag = "601";
            this.radOnline.Text = "Online";
            this.radOnline.UseVisualStyleBackColor = true;
            this.radOnline.CheckedChanged += new System.EventHandler(this.RadOnline_CheckedChanged);
            // 
            // radOffline
            // 
            this.radOffline.AutoSize = true;
            this.radOffline.Location = new System.Drawing.Point(394, 76);
            this.radOffline.Margin = new System.Windows.Forms.Padding(6);
            this.radOffline.Name = "radOffline";
            this.radOffline.Size = new System.Drawing.Size(93, 29);
            this.radOffline.TabIndex = 7;
            this.radOffline.Tag = "602";
            this.radOffline.Text = "Offline";
            this.radOffline.UseVisualStyleBackColor = true;
            this.radOffline.CheckedChanged += new System.EventHandler(this.RadOnline_CheckedChanged);
            // 
            // Panel600
            // 
            this.Panel600.Controls.Add(this.Panel600Button);
            this.Panel600.Controls.Add(this.Panel6Label);
            this.Panel600.Controls.Add(this.txtOfflineUrl);
            this.Panel600.Controls.Add(this.txtOnlineUrl);
            this.Panel600.Controls.Add(this.LocationLabel);
            this.Panel600.Controls.Add(this.label1);
            this.Panel600.Controls.Add(this.radOnline);
            this.Panel600.Controls.Add(this.radOffline);
            this.Panel600.Location = new System.Drawing.Point(820, 447);
            this.Panel600.Margin = new System.Windows.Forms.Padding(6);
            this.Panel600.Name = "Panel600";
            this.Panel600.Size = new System.Drawing.Size(868, 258);
            this.Panel600.TabIndex = 8;
            this.Panel600.Tag = "600";
            // 
            // Panel600Button
            // 
            this.Panel600Button.Location = new System.Drawing.Point(713, 122);
            this.Panel600Button.Name = "Panel600Button";
            this.Panel600Button.Size = new System.Drawing.Size(141, 43);
            this.Panel600Button.TabIndex = 12;
            this.Panel600Button.Text = "Select Folder";
            this.Panel600Button.UseVisualStyleBackColor = true;
            this.Panel600Button.Click += new System.EventHandler(this.ShowFolderSelection);
            // 
            // Panel6Label
            // 
            this.Panel6Label.AutoSize = true;
            this.Panel6Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel6Label.Location = new System.Drawing.Point(6, 0);
            this.Panel6Label.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.Panel6Label.Name = "Panel6Label";
            this.Panel6Label.Size = new System.Drawing.Size(233, 39);
            this.Panel6Label.TabIndex = 2;
            this.Panel6Label.Text = "Configuration";
            // 
            // txtOfflineUrl
            // 
            this.txtOfflineUrl.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.txtOfflineUrl.Location = new System.Drawing.Point(227, 128);
            this.txtOfflineUrl.Margin = new System.Windows.Forms.Padding(6);
            this.txtOfflineUrl.Name = "txtOfflineUrl";
            this.txtOfflineUrl.ReadOnly = true;
            this.txtOfflineUrl.Size = new System.Drawing.Size(475, 29);
            this.txtOfflineUrl.TabIndex = 11;
            this.txtOfflineUrl.Tag = "603";
            this.txtOfflineUrl.Enter += new System.EventHandler(this.ShowFolderSelection);
            // 
            // txtOnlineUrl
            // 
            this.txtOnlineUrl.Location = new System.Drawing.Point(227, 128);
            this.txtOnlineUrl.Margin = new System.Windows.Forms.Padding(6);
            this.txtOnlineUrl.Name = "txtOnlineUrl";
            this.txtOnlineUrl.Size = new System.Drawing.Size(475, 29);
            this.txtOnlineUrl.TabIndex = 10;
            this.txtOnlineUrl.Tag = "604";
            // 
            // LocationLabel
            // 
            this.LocationLabel.AutoSize = true;
            this.LocationLabel.Location = new System.Drawing.Point(31, 128);
            this.LocationLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LocationLabel.Name = "LocationLabel";
            this.LocationLabel.Size = new System.Drawing.Size(147, 25);
            this.LocationLabel.TabIndex = 9;
            this.LocationLabel.Text = "Online location:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 76);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 25);
            this.label1.TabIndex = 8;
            this.label1.Text = "Help source:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1718, 958);
            this.Controls.Add(this.Panel600);
            this.Controls.Add(this.panel500);
            this.Controls.Add(this.panel400);
            this.Controls.Add(this.panel300);
            this.Controls.Add(this.panel200);
            this.Controls.Add(this.panel100);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.helpProvider.SetShowHelp(this, false);
            this.Text = "Help Demo";
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.MainForm_ShowHelp);
            this.panel100.ResumeLayout(false);
            this.panel100.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.panel200.ResumeLayout(false);
            this.panel200.PerformLayout();
            this.panel300.ResumeLayout(false);
            this.panel300.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Panel3NumericUpDown)).EndInit();
            this.panel400.ResumeLayout(false);
            this.panel400.PerformLayout();
            this.panel500.ResumeLayout(false);
            this.panel500.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.Panel600.ResumeLayout(false);
            this.Panel600.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel100;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem menu1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menu2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menu3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menu4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menu5ToolStripMenuItem;
        private System.Windows.Forms.Panel panel200;
        private System.Windows.Forms.Panel panel300;
        private System.Windows.Forms.Panel panel400;
        private System.Windows.Forms.Panel panel500;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.HelpProvider helpProvider;
        private System.Windows.Forms.ToolStripMenuItem subMenu51ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subMenu52ToolStripMenuItem;
        private System.Windows.Forms.RadioButton radOnline;
        private System.Windows.Forms.RadioButton radOffline;
        private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
        private System.Windows.Forms.Panel Panel600;
        private System.Windows.Forms.TextBox txtOnlineUrl;
        private System.Windows.Forms.Label LocationLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtOfflineUrl;
        private System.Windows.Forms.Button Panel1Button;
        private System.Windows.Forms.ComboBox Panel1ComboBox;
        private System.Windows.Forms.Label Panel1Label;
        private System.Windows.Forms.Label Panel2Label;
        private System.Windows.Forms.Label Panel3Label;
        private System.Windows.Forms.Label Panel4Label;
        private System.Windows.Forms.Label Panel5Label;
        private System.Windows.Forms.Label Panel6Label;
        private System.Windows.Forms.TextBox Panel2TextBox;
        private System.Windows.Forms.CheckedListBox Panel2CheckedListBox;
        private System.Windows.Forms.RichTextBox Panel4RichTextBox;
        private System.Windows.Forms.RadioButton Panel4RadioButton2;
        private System.Windows.Forms.RadioButton Panel4RadioButton1;
        private System.Windows.Forms.MonthCalendar Panel5MonthCalendar;
        private System.Windows.Forms.NumericUpDown Panel3NumericUpDown;
        private System.Windows.Forms.ListBox Panel3ListBox;
        private System.Windows.Forms.Button Panel600Button;
    }
}

