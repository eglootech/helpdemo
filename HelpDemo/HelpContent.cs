﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Windows.Forms;

namespace HelpDemo
{
    /// <summary>
    /// Class to manage configuration and access to 
    /// help content provided by PageSeeder.
    /// </summary>
    public class HelpContent
    {
        /// <summary>
        /// Cache key for the Topic mapping lookups.
        /// </summary>
        private const string TOPIC_LOOKUP_KEY = "TopicId-Lookup";

        /// <summary>
        /// AppSettings key for configuring the base online Help URL.
        /// </summary>
        private const string CFG_HELPURL_KEY = "HelpUrl";
        
        /// <summary>
        /// AppSettings key for configuring the base local file system path for Help files.
        /// </summary>
        private const string CFG_HELPPATH_KEY = "HelpPath";

        /// <summary>
        /// AppSettings key for configuring the name of the Topic mapping file.
        /// </summary>
        private const string CFG_HELPMAPPINGFILE_KEY = "HelpMappingFile";

        /// <summary>
        /// Instance of Memory cache for storing <see cref="_TopicMapping"/>
        /// </summary>
        private MemoryCache _Cache = MemoryCache.Default;

        /// <summary>
        /// Configuration setting for name of mapping file.
        /// File is expected to exist in application folder.
        /// </summary>
        private string _MappingFileNameConfig
        {
            get
            {
                string result = ConfigurationManager.AppSettings[CFG_HELPMAPPINGFILE_KEY];
                if (string.IsNullOrEmpty(result))
                {
                    result = "Map.h";
                }
                return result;
            }
        }

        /// <summary>
        /// Configuration setting for the location of online help.
        /// </summary>
        private string _HelpUrlConfig
        {
            get
            {
                string result = ConfigurationManager.AppSettings[CFG_HELPURL_KEY];
                if (string.IsNullOrEmpty(result))
                {
                    result = "https://teradyne.pageseeder.net";
                }
                return result;
            }
        }

        /// <summary>
        /// Configuration setting for the location of offline help files.
        /// </summary>
        private string _HelpPathConfig
        {
            get
            {
                string result = ConfigurationManager.AppSettings[CFG_HELPPATH_KEY];
                if (string.IsNullOrEmpty(result))
                {
                    result = @"C:\Temp";
                }
                return result;
            }
        }

        /// <summary>
        /// Cached lookup of Topic Ids from parsed Map.h file.
        /// </summary>
        private Hashtable _TopicMapping
        {
            get
            {
                if (!(_Cache[TOPIC_LOOKUP_KEY] is Hashtable lookup))
                {
                    lookup = new Hashtable();
                    CacheItemPolicy policy = new CacheItemPolicy();

                    if (HelpMappingFile.Exists)
                    {
                        policy.ChangeMonitors.Add(new HostFileChangeMonitor(new [] { HelpMappingFile.FullName }));

                        foreach (string line in File.ReadLines(HelpMappingFile.FullName))
                        {
                            if (line.Trim().StartsWith("#define", StringComparison.CurrentCultureIgnoreCase))
                            {
                                var columns = line.Split(null);
                                if (columns.Count() >= 3)
                                {
                                    lookup.Add(columns[2], columns[1]);
                                }
                            }
                        }
                    }

                    _Cache.Set(TOPIC_LOOKUP_KEY, lookup, policy);
                }

                return lookup;
            }
        }

        /// <summary>
        /// Delegated function to determine if online or offline help is required.
        /// </summary>
        private Func<bool> _IsOnline;

        /// <summary>
        /// Topic mapping file parsed to create <see cref="_TopicMapping"/>
        /// <seealso cref="_MappingFileNameConfig"/>
        /// </summary>
        public FileInfo HelpMappingFile { get; }

        /// <summary>
        /// Url for online help contents.
        /// <seealso cref="_HelpUrlConfig"/>
        /// </summary>
        public string HelpUrl { get; }

        /// <summary>
        /// Local file path for offline help contents.
        /// <seealso cref="_HelpPathConfig"/>
        /// </summary>
        public string HelpPath { get; }

        /// <summary>
        /// Manage integration of online and offline help content that
        /// replaces logic provided by standard <see cref="System.Windows.Forms.Help"/>
        /// </summary>
        /// <param name="isOnline"></param>
        public HelpContent(Func<bool> isOnline)
        {
            _IsOnline = isOnline;

            HelpMappingFile = new FileInfo(_MappingFileNameConfig);
            HelpUrl = _HelpUrlConfig;
            HelpPath = _HelpPathConfig;
        }

        /// <summary>
        /// Return URL or local path for help content based on <see cref="_IsOnline"/>
        /// <para>Help topic is determined by call to <see cref="GetHelpTopic"/></para>
        /// </summary>
        /// <param name="contextId">Context ID which maps to help resource.</param>
        /// <returns></returns>
        public string GetTopicHelpLocation(string contextId)
        {
            string basePath = _IsOnline() ? HelpUrl : HelpPath;
            string topic = GetHelpTopic(contextId);

            if (!string.IsNullOrEmpty(topic))
            {
                return Path.Combine(basePath, $"topics{topic}.html");
            }
            else
            {
                return Path.Combine(basePath, "index.html");
            }
        }

        /// <summary>
        /// Find topic reference for Context ID from <see cref="_TopicMapping"/>
        /// </summary>
        /// <param name="contextId">Context ID which maps to help resource.</param>
        /// <returns></returns>
        public string GetHelpTopic(string contextId)
        {
            return _TopicMapping.ContainsKey(contextId) ? _TopicMapping[contextId].ToString() : string.Empty;
        }

        /// <summary>
        /// Recurse through controls to disable built-in F1 help.
        /// </summary>
        /// <param name="c"></param>
        public void InitialiseHelp(HelpProvider helpProvider, Control control)
        {
            try
            {
                helpProvider.SetShowHelp(control, false);
            }
            catch
            {
                // allow child controls to be initialised
            }

            foreach (Control child in control.Controls)
            {
                InitialiseHelp(helpProvider, child);
            }
        }

    }
}
