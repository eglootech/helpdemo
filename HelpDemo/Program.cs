﻿using System;
using System.Windows.Forms;

namespace HelpDemo
{
    static class Program
    {
        /// <summary>
        /// Entry point for the Teradyne Help demonstration application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
