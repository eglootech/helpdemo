﻿#define docid contextId // comment
#define tr-39053 100 // About
#define tr-39058 101 // Features overview
#define tr-39059 102 // Safety information
#define tr-39054 200 // Theory of operation
#define tr-39062 201 // Board Hardware
#define tr-39063 202 // Test Head Connections
#define tr-39055 300 // DCVS Programming
#define tr-39070 301 // Simulated configuration files
#define tr-39071 302 // Programming procedure overview
#define tr-39056 400 // Dislay debugging
#define tr-39114 401 // Starting DCVS Debug Display
#define XXXX 402 // DCVS Debug Display
#define tr-39115 403
#define tr-39057 500
#define tr-39079 501
#define tr-39118 511
#define tr-39077 512
#define tr-39078 600
#define tr-39073 601
#define tr-39074 602
#define tr-39075 603
#define tr-39076 604
#define xxxx 700 // Reset values
