﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

public static class Extensions
{
    /// <summary>
    /// Extension method to return currently selected items from <see cref="Descendants(MenuStrip)"/>.
    /// </summary>
    /// <param name="menu"></param>
    /// <returns></returns>
    public static ToolStripMenuItem SelectedItem(this MenuStrip menu)
    {
        return menu.Descendants().Where(x => x.Selected).FirstOrDefault();
    }

    /// <summary>
    /// Extension method to recurisvely return collection of child <see cref="ToolStripMenuItem"/> items.
    /// </summary>
    /// <param name="menu"></param>
    /// <returns></returns>
    public static List<ToolStripMenuItem> Descendants(this MenuStrip menu)
    {
        var items = menu.Items.OfType<ToolStripMenuItem>().ToList();
        return items.SelectMany(x => Descendants(x)).Concat(items).ToList();
    }

    /// <summary>
    /// Extension method to recursively return collection of child <see cref="ToolStripMenuItem"/> items.
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public static List<ToolStripMenuItem> Descendants(this ToolStripMenuItem item)
    {
        var items = item.DropDownItems.OfType<ToolStripMenuItem>().ToList();
        return items.SelectMany(x => Descendants(x)).Concat(items).ToList();
    }
}